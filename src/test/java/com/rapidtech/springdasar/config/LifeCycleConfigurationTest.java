package com.rapidtech.springdasar.config;

import com.rapidtech.springdasar.lifeCycle.Connection;
import com.rapidtech.springdasar.lifeCycle.LifeCycleConfiguration;
import com.rapidtech.springdasar.lifeCycle.Server;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class LifeCycleConfigurationTest {
    private ConfigurableApplicationContext context;
    @BeforeEach
    void setUp() {
        context = new AnnotationConfigApplicationContext(LifeCycleConfiguration.class);
    }

    @AfterEach
    void tearDown() {
        context.close();
    }


    @Test
    void lifeCycleTest() {
//        ApplicationContext context
//                = new AnnotationConfigApplicationContext(LifeCycleConfiguration.class);

//        ConfigurableApplicationContext context
//                = new AnnotationConfigApplicationContext(LifeCycleConfiguration.class);

        Connection connection = context.getBean(Connection.class);

//        context.close();
    }

    @Test
    void serverTest() {
        Server server = context.getBean(Server.class);
    }
}
