package com.rapidtech.springdasar.config;

import com.rapidtech.springdasar.cyclic.CylicConfiguration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.*;

class CylicConfigurationTest {
    private ApplicationContext context;
    @BeforeEach
    void setUp() {
//        try {
//            context = new AnnotationConfigApplicationContext(CylicConfiguration.class);
//
//            Assertions.fail("It must be fail because cyclic");
//
//        } catch (BeansException exception){
//            exception.printStackTrace();
//        }
        Assertions.assertThrows(Throwable.class, () -> {
            context = new AnnotationConfigApplicationContext(CylicConfiguration.class);
        });
    }

    @Test
    void circularTest() {

    }
}

